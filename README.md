## Introduction

This is the adaptation to Elasticsearch/Kibana (with X-pack) 5.6, Flink 1.4 & some parts of our PoC stack (HDFS & Kafka) of the following article: https://www.elastic.co/blog/building-real-time-dashboard-applications-with-apache-flink-elasticsearch-and-kibana

A Flink application project using Scala and SBT.

Normally to run and test your application locally, you can just execute `sbt run` then select the main class that contains the Flink job. Unfortunately this project reads input data from HDFS & puts it to Elasticsearch after several transformations. Thats why the quickest way to run it is to use our PoC stack.

You can package the application into a fat jar with `sbt assembly`, then submit it as usual, with something like 
**(TODO: should be from within docker container pointing to remote flink job manager within stack):**

```
flink run -c org.example.WordCount /path/to/your/project/my-app/target/scala-2.11/testme-assembly-0.1-SNAPSHOT.jar
```


#### Idea config in orser to build

You can also run your application from within IntelliJ:  select the classpath of the 'mainRunner' module in the run/debug configurations.
Simply open 'Run -> Edit configurations...' and then select 'mainRunner' from the "Use classpath of module" dropbox. 

#### ES X-pack security

https://www.elastic.co/guide/en/x-pack/5.6/security-getting-started.html

## Running job on PoC stack

1. Copy source taxi fares data into HDFS:

```bash
docker run -it --rm --env-file=./hadoop-hive.env --volume $PWD/taxi_data:/data --network lizard2 bde2020/hadoop-namenode:2.0.0-hadoop2.7.4-java8 hadoop fs -mkdir /user/flink/taxi_fares/

docker run -it --rm --env-file=./hadoop-hive.env --volume $PWD/taxi_data:/data --network lizard2 bde2020/hadoop-namenode:2.0.0-hadoop2.7.4-java8 hadoop fs -copyFromLocal /data/nycTaxiData.gz /user/flink/taxi_fares/
```

2. Create index in Elasticsearch:

```bash
curl --user elastic:changeme -XPUT "http://localhost:9200/nyc-idx"

curl --user elastic:changeme -XPUT "http://localhost:9200/nyc-idx/_mapping/popular-locations" -d' { "popular-locations" : { "properties" : { "cnt": {"type": "integer"}, "location": {"type": "geo_point"}, "time": {"type": "date"}}}}'
```

3. (Optional) Listen to debug Kafka topic where all messages go as well as to Elasticsearch:

```bash
docker run -it --rm --network lizard2 -e KAFKA_ZOOKEEPER_CONNECT="zookeeper:2181" bitnami/kafka:0.11.0-1-r1 kafka-console-consumer.sh --bootstrap-server kafka:9092 --topic taxi_fares_cnt_debug
```

4. Assemble fat jar with job:

```bash
sbt clean assembly
```

5. Upload jar to Flink (see `docker-fdatapoc` for instructions)

6. Set up Kibana visualization (TODO)

Kibana login: elastic/changeme


