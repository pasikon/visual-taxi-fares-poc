ThisBuild / resolvers ++= Seq(
    "Apache Development Snapshot Repository" at "https://repository.apache.org/content/repositories/snapshots/",
    Resolver.mavenLocal
)

resolvers in ThisBuild += "elastic" at "https://artifacts.elastic.co/maven"

name := "visual-taxi-fares-poc"

version := "0.1-SNAPSHOT"

organization := "org.fdatapoc"

ThisBuild / scalaVersion := "2.11.12"

val flinkVersion = "1.4.2"

val flinkDependencies = Seq(
  "org.apache.flink" %% "flink-scala" % flinkVersion % "provided",
  "org.apache.flink" %% "flink-streaming-scala" % flinkVersion % "provided",
  "org.apache.flink" %% "flink-hadoop-compatibility" % flinkVersion % "provided",
  "org.apache.flink" %% "flink-connector-kafka-0.11" % flinkVersion
//  "org.apache.flink" %% "flink-connector-elasticsearch5" % flinkVersion
)

libraryDependencies ++= Seq(
  "joda-time" % "joda-time" % "2.7",
  "org.apache.logging.log4j" % "log4j" % "2.11.0",
  "org.apache.logging.log4j" % "log4j-api" % "2.11.0",
  "org.apache.logging.log4j" % "log4j-slf4j-impl" % "2.11.0",
  "org.apache.kafka" % "kafka-clients" % "0.11.0.2",
  "org.elasticsearch.client" % "x-pack-transport" % "5.6.8"
)

lazy val root = (project in file(".")).
  settings(
    libraryDependencies ++= flinkDependencies
  )

assembly / mainClass := Some("org.fdatapoc.VisTaxiFares")

// make run command include the provided dependencies
Compile / run  := Defaults.runTask(Compile / fullClasspath,
                                   Compile / run / mainClass,
                                   Compile / run / runner
                                  ).evaluated

// stays inside the sbt console when we press "ctrl-c" while a Flink programme executes with "run" or "runMain"
Compile / run / fork := true
Global / cancelable := true

// exclude Scala library from assembly
assembly / assemblyOption  := (assembly / assemblyOption).value.copy(includeScala = false)

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}
