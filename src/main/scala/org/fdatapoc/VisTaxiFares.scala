package org.fdatapoc

import java.net.{InetAddress, InetSocketAddress}

import org.apache.flink.api.common.functions.{FilterFunction, RuntimeContext}
import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.datastream.{DataStreamSource, SingleOutputStreamOperator}
import org.apache.flink.streaming.api.functions.source.SourceFunction
import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment}
import org.fdatapoc.datatypes.{GeoPoint, TaxiRide}
import org.fdatapoc.source.TaxiRideSource
import org.fdatapoc.utils.NycGeoUtils
import org.apache.flink.streaming.api.scala._
//import org.apache.flink.streaming.connectors.elasticsearch.{ElasticsearchSinkFunction, RequestIndexer}
//import org.apache.flink.streaming.connectors.elasticsearch5.ElasticsearchSink
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.client.Requests
import org.fdatapoc.sink.ElasticsearchUpsertSink

object VisTaxiFares {

  def main(args: Array[String]): Unit = {

    // input parameters
    val data = "taxi_fares/"
    val maxServingDelay = 60
    val servingSpeedFactor = 600f

    // Elasticsearch parameters
    val elasticsearchHost = "elasticsearch" // look-up hostname in Elasticsearch log output
    val elasticsearchPort = 9300

    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)

    val source: SourceFunction[TaxiRide] = new TaxiRideSource(
      data, maxServingDelay, servingSpeedFactor)
    // Define the data source
    val rides: DataStream[TaxiRide] = env.addSource(source)

    val cleansedRides: DataStream[TaxiRide] = rides.filter(new FilterFunction[TaxiRide] {
      override def filter(value: TaxiRide): Boolean = !value.isStart
    })
      // filter for events in NYC
      .filter(new FilterFunction[TaxiRide] {
      override def filter(value: TaxiRide): Boolean = NycGeoUtils.isInNYC(value.location)
    })

    // map location coordinates to cell Id, timestamp, and passenger count
    val cellIds: DataStream[(Int, Long, Short)] = cleansedRides.map(r =>( NycGeoUtils.mapToGridCell(r.location), r.time.getMillis, r.passengerCnt ))

    val xox: KeyedStream[(Int, Long, Short), Int] = cellIds
      // key stream by cell Id
      .keyBy(_._1)

    val passengerCnts: DataStream[(Int, Long, Int)] =
      xox.fold(0, 0L, 0)((s: (Int, Long, Int), r: (Int, Long, Short)) => (r._1, s._2.max(r._2), s._3 + r._3))

    // map cell Id back to GeoPoint
    val cntByLocation: DataStream[(Int, Long, GeoPoint, Int)] = passengerCnts
      .map( r => (r._1, r._2, NycGeoUtils.getGridCellCenter(r._1), r._3 ) )

    // print to console
    cntByLocation
      .print()

//    getEs5Sink
//    cntByLocation.addSink(es5sink)

    cntByLocation.addSink(new CntTimeByLocUpsert(elasticsearchHost, elasticsearchPort))

    cntByLocation.map(r => s"location: ${r._3.lat + "," + r._3.lon}, time: ${r._2}, count: ${r._4}").addSink(
      new FlinkKafkaProducer011[String]("kafka:9092", "taxi_fares_cnt_debug", new SimpleStringSchema())
    )

//    if (writeToElasticsearch) {
//      // write to Elasticsearch
//      cntByLocation
//        .addSink(new CntTimeByLocUpsert(elasticsearchHost, elasticsearchPort))
//    }

    env.execute("Total passenger count per location")

  }

//  private def getEs5Sink = {
//    val config = new java.util.HashMap[String, String]
//    config.put("cluster.name", "my-cluster-name")
//    // This instructs the sink to emit after every element, otherwise they would be buffered
//    config.put("bulk.flush.max.actions", "1")
//
//    val transportAddresses = new java.util.ArrayList[InetSocketAddress]
//    transportAddresses.add(new InetSocketAddress(InetAddress.getByName("elasticsearch"), 9300))
//
//    val es5sink = new ElasticsearchSink(config, transportAddresses, new ElasticsearchSinkFunction[(Int, Long, GeoPoint, Int)] {
//      def createIndexRequest(r: (Int, Long, GeoPoint, Int)): IndexRequest = {
//        val json = Map(
//          "location" -> (r._3.lat + "," + r._3.lon).asInstanceOf[AnyRef],
//          "time" -> r._2.asInstanceOf[AnyRef],
//          "cnt" -> r._4.asInstanceOf[AnyRef]
//        )
//
//        //        Requests.up
//        Requests.indexRequest().index("nyc-idx").`type`("popular-locations").source(json)
//      }
//
//      override def process(element: (Int, Long, GeoPoint, Int), runtimeContext: RuntimeContext, indexer: RequestIndexer): Unit = {
//        indexer.add(createIndexRequest(element))
//      }
//    })
//  }

  class CntTimeByLocUpsert(host: String, port: Int)
    extends ElasticsearchUpsertSink[(Int, Long, GeoPoint, Int)](
      host,
      port,
      "elasticsearch",
      "nyc-idx",
      "popular-locations") {

    override def insertJson(r: (Int, Long, GeoPoint, Int)): Map[String, AnyRef] = {
      Map(
        "location" -> (r._3.lat+","+r._3.lon).asInstanceOf[AnyRef],
        "time" -> r._2.asInstanceOf[AnyRef],
        "cnt" -> r._4.asInstanceOf[AnyRef]
      )
    }

    override def updateJson(r: (Int, Long, GeoPoint, Int)): Map[String, AnyRef] = {
      Map[String, AnyRef] (
        "time" -> r._2.asInstanceOf[AnyRef],
        "cnt" -> r._4.asInstanceOf[AnyRef]
      )
    }

    override def indexKey(r: (Int, Long, GeoPoint, Int)): String = {
      // index by location
      r._1.toString
    }
  }

}
